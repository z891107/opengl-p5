#include "libs.h"

Vertex vertices[] = {
	//Position						//Color						//Texcoord
	glm::vec3(-0.5f, -0.3f, 0.5f),		glm::vec3(1.f, 0.f, 0.f),	glm::vec2(0.f, 1.f),
	glm::vec3(-0.5f, -0.3f, -0.5f),	glm::vec3(0.f, 1.f, 0.f),	glm::vec2(0.f, 0.f),
	glm::vec3(0.5f, -0.3f, -0.5f),	glm::vec3(0.f, 0.f, 1.f),	glm::vec2(1.f, 0.f),
	glm::vec3(0.5f, -0.3f, 0.5f),	glm::vec3(0.f, 0.f, 1.f),	glm::vec2(1.f, 1.f)
};
size_t verticesSize = sizeof(vertices) / sizeof(Vertex);

std::vector<Vertex> floorV;
std::vector<GLfloat> floorVertices;
std::vector<GLfloat> floorTexcoord;

GLuint indices[] = {
	0, 1, 2,
	0, 2, 3
};
size_t indicesSize = sizeof(indices) / sizeof(GLuint);

std::vector<glm::vec3> treeCentral;

glm::vec3 camPosition(0.f, 0.f, 1.f);
glm::vec3 worldUp(0.f, 1.f, 0.f);
float camYaw = 270.f;
float camPitch = 0.f;
glm::vec3 camFront(sin(glm::radians(camYaw)), 0.f, -cos(glm::radians(camYaw)));
glm::mat4 viewMatrix(1.f);

int delay = 0;
int time = 0;
int timeDiff = 1;

void pollInput(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		camPosition += glm::vec3(camFront.x, 0.f, camFront.z) * 0.02f;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		camPosition += glm::vec3(glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f)) * glm::vec4(camFront.x, 0.f, camFront.z, 1.f) * 0.02f);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		camPosition += glm::vec3(glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f)) * glm::vec4(camFront.x, 0.f, camFront.z, 1.f) * 0.02f);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		camPosition += glm::vec3(glm::rotate(glm::mat4(1.f), glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f)) * glm::vec4(camFront.x, 0.f, camFront.z, 1.f) * 0.02f);
	}

	if (camPosition.x > 10.f) {
		camPosition.x = 10.f;
	}
	if (camPosition.z > 10.f) {
		camPosition.z = 10.f;
	}
	if (camPosition.x < -10.f) {
		camPosition.x = -10.f;
	}
	if (camPosition.z < -10.f) {
		camPosition.z = -10.f;
	}

	if (delay == 0) {
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
			delay = 30;
			treeCentral.push_back(camPosition + camFront);
		}
	}
	else {
		delay--;
	}

	if (time >= 100 || time <= -100) {
		timeDiff = -timeDiff;
	}

	time += timeDiff;
}

bool firstMouse = true;
float lastX, lastY;

void moveMouse_Callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos;
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.1f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	camYaw += xoffset;
	camPitch += yoffset;

	if (camPitch > 89.0f)
		camPitch = 89.0f;
	if (camPitch < -89.0f)
		camPitch = -89.0f;

	glm::vec3 direction;
	direction.x = cos(glm::radians(camYaw)) * cos(glm::radians(camPitch));
	direction.y = sin(glm::radians(camPitch));
	direction.z = sin(glm::radians(camYaw)) * cos(glm::radians(camPitch));
	camFront = glm::normalize(direction);
}

void ResizeFramebuffer_Callback(GLFWwindow* w, int fbw, int fbh) {
	glViewport(0, 0, fbw, fbh);
}

void generateFloorVertices() {
	int floorSize = 400;
	for (int y = 0; y <= floorSize; y++) {
		GLfloat verticesSegY = vertices[0].position.z + (vertices[1].position.z - vertices[0].position.z) / floorSize * y;
		//GLfloat normalSegY = start_end_normal[3] + (start_end_normal[6] - start_end_normal[3]) / segSize * y;
		GLfloat texture_coordinateSegY = vertices[0].texcoord.y + (vertices[1].texcoord.y - vertices[0].texcoord.y) / floorSize * y;

		for (int x = 0; x <= floorSize; x++) {
			GLfloat verticesSegX = vertices[0].position.x + (vertices[2].position.x - vertices[0].position.x) / floorSize * x;
			//GLfloat normalSegX = start_end_normal[2] + (start_end_normal[5] - start_end_normal[2]) / segSize * x;
			GLfloat texture_coordinateSegX = vertices[0].texcoord.x + (vertices[2].texcoord.x - vertices[0].texcoord.x) / floorSize * x;

			floorV.push_back({ glm::vec3(verticesSegX , vertices[0].position.y, verticesSegY), glm::vec3(0.f), glm::vec2(texture_coordinateSegX, texture_coordinateSegY) });
		}
	}
}

unsigned int loadCubemap(std::vector<std::string> faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}

int main() {
	// Init GLFW
	glfwInit();
	
	// Create Window
	const int WINDOW_WIDTH = 1920;
	const int WINDOW_HEIGHT = 1280;
	int framebufferWidth = 0;
	int framebufferHeight = 0;

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "TEST_WINDOW", NULL, NULL);

	glfwSetFramebufferSizeCallback(window, ResizeFramebuffer_Callback);
	glfwGetFramebufferSize(window, &framebufferWidth, &framebufferHeight);
	
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(window, moveMouse_Callback);

	glfwMakeContextCurrent(window);

	// Init GLEW (Need window and OpenGL context)
	glewExperimental = GL_TRUE;

	// Error
	if (glewInit() != GLEW_OK) {
		std::cout << "ERROR::MAIN.CPP::GLEW_INIT_FAILED\n";
		glfwTerminate();
	}
	
	// OpenGL options
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	//glFrontFace(GL_CCW);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // GL_FILL is default

	generateFloorVertices();

	// Shader init
	Shader simpleShader("Shader/simple.vert", "Shader/simple.frag");
	Shader skyboxShader("Shader/skybox.vert", "Shader/skybox.frag");
	Shader colorShader("Shader/simple.vert", "Shader/color.frag");

	// VAO, VBO, EBO
	// Gen VAO and bind
	GLuint VAO;
	glCreateVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// Gen VBO and bind and send data
	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Gen EBO and bind and send data
	GLuint EBO;
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Set vertexAttrbPointers and enable (Input assembly)
	// Position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
	glEnableVertexAttribArray(0);
	// Color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));
	glEnableVertexAttribArray(1);
	// Position
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));
	glEnableVertexAttribArray(2);

	// Unbind VAO
	glBindVertexArray(0);

	// skybox VAO
	unsigned int skyboxVAO, skyboxVBO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	// cube VAO
	unsigned int cubeVAO, cubeVBO;
	glGenVertexArrays(1, &cubeVAO);
	glGenBuffers(1, &cubeVBO);
	glBindVertexArray(cubeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	
	// Texture init
	int image_width = 0;
	int image_height = 0;
	unsigned char* image = SOIL_load_image("Image/floor.jpg", &image_width, &image_height, NULL, SOIL_LOAD_RGBA);

	GLuint texture0;
	glGenTextures(1, &texture0);
	glBindTexture(GL_TEXTURE_2D, texture0);
	
	// (S mean x coord, T mean y coord)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (image) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		std::cout << "ERROR::TEXTURE_LOADING_FAILED\n";
	}

	SOIL_free_image_data(image);

	image = SOIL_load_image("Image/wood.jpg", &image_width, &image_height, NULL, SOIL_LOAD_RGBA);

	GLuint woodTexture;
	glGenTextures(1, &woodTexture);
	glBindTexture(GL_TEXTURE_2D, woodTexture);

	// (S mean x coord, T mean y coord)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (image) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		std::cout << "ERROR::TEXTURE_LOADING_FAILED\n";
	}

	SOIL_free_image_data(image);

	image = SOIL_load_image("Image/leave.png", &image_width, &image_height, NULL, SOIL_LOAD_RGBA);

	GLuint leaveTexture;
	glGenTextures(1, &leaveTexture);
	glBindTexture(GL_TEXTURE_2D, leaveTexture);

	// (S mean x coord, T mean y coord)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (image) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		std::cout << "ERROR::TEXTURE_LOADING_FAILED\n";
	}

	std::vector<std::string> faces
	{
		"Image/Skybox/right.png",
		"Image/Skybox/left.png",
		"Image/Skybox/topp.png",
		"Image/Skybox/bottom.png",
		"Image/Skybox/back.png",
		"Image/Skybox/front.png"
	};
	unsigned int cubemapTexture = loadCubemap(faces);

	// Unbind texture
	//glActiveTexture(0);
	//glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);

	// Matrices init
	glm::mat4 modelMatrix(1.f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.f, 0.f, 0.f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(1.f, 0.f, 0.f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 1.f, 0.f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 0.f, 1.f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(1.f));
	
	viewMatrix = glm::lookAt(camPosition, camPosition + camFront, worldUp);

	float fov = 90.f;
	float nearPlane = 0.1f;
	float farPlane = 1000.f;
	std::cout << static_cast<float>(framebufferWidth) / framebufferHeight;
	glm::mat4 projectionMatrix = glm::perspective(
		glm::radians(fov),
		static_cast<float>(framebufferWidth) / framebufferHeight,
		nearPlane,
		farPlane
	);

	skyboxShader.use();
	skyboxShader.setInt("skybox", 0);

	// Main loop
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		pollInput(window);

		// Clear frameBuffer
		glClearColor(0.f, 0.f, 0.f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
		// Use a shader
		simpleShader.use();

		viewMatrix = glm::lookAt(camPosition, camPosition + camFront, worldUp);
		simpleShader.setMat4("view", viewMatrix);

		glfwGetFramebufferSize(window, &framebufferWidth, &framebufferHeight);
		projectionMatrix = glm::perspective(
			glm::radians(fov),
			static_cast<float>(framebufferWidth) / framebufferHeight,
			nearPlane,
			farPlane
		);
		simpleShader.setMat4("projection", projectionMatrix);

		// Activate texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture0);

		// Bind VAO
		glBindVertexArray(VAO);
		for (float z = -10.f; z < 11.f; z+=0.5f) {
			for (float x = -10.f; x < 11.f; x+=0.5f) {
					modelMatrix = glm::mat4(1.f);
					modelMatrix = glm::translate(modelMatrix, glm::vec3(x, 0.f, z));
					modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(1.f, 0.f, 0.f));
					modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 1.f, 0.f));
					modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 0.f, 1.f));
					modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
					simpleShader.setMat4("model", modelMatrix);

					// Draw
					//glDrawArrays(GL_TRIANGLES, 0, floorV.size());
					glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_INT, 0);
			}
		}

		// Activate texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, woodTexture);

		// Bind VAO
		glBindVertexArray(cubeVAO);
		for (auto& point : treeCentral) {
			float x = point.x;
			float z = point.z;

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, woodTexture);

				for (int y = 0; y < 3; y++) {
					modelMatrix = glm::mat4(1.f);
					modelMatrix = glm::translate(modelMatrix, glm::vec3(x, y / 2.f, z));
					modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(1.f, 0.f, 0.f));
					modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 1.f, 0.f));
					modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 0.f, 1.f));
					modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
					simpleShader.setMat4("model", modelMatrix);

					// Draw
					glDrawArrays(GL_TRIANGLES, 0, 36);
				}

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, leaveTexture);

				

				for (int zz = -2; zz < 3; zz++) {
					for (int xx = -2; xx < 3; xx++) {
						float random1 = (1 - rand() % 3) * 0.002;
						float random2 = (1 - rand() % 3) * 0.002;
						modelMatrix = glm::mat4(1.f);
						modelMatrix = glm::translate(modelMatrix, glm::vec3(xx * 0.5f + x + (-((time * 0.001) * (time * 0.01) * (time * 0.01))) + random1, 1.5f, zz * 0.5f + z + (-((time * 0.001) * (time * 0.01) * (time * 0.01))) + random2));
						modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(1.f, 0.f, 0.f));
						modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 1.f, 0.f));
						modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 0.f, 1.f));
						modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
						simpleShader.setMat4("model", modelMatrix);

						// Draw
						glDrawArrays(GL_TRIANGLES, 0, 36);

						if (zz >= -1 && zz <= 1 && xx >= -1 && xx <= 1) {
							modelMatrix = glm::mat4(1.f);
							modelMatrix = glm::translate(modelMatrix, glm::vec3(xx * 0.5f + x + (-((time * 0.001) * (time * 0.01) * (time * 0.01))) + random1, 2.f, zz * 0.5f + z + (-((time * 0.001) * (time * 0.01) * (time * 0.01))) + random2));
							modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(1.f, 0.f, 0.f));
							modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 1.f, 0.f));
							modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 0.f, 1.f));
							modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
							simpleShader.setMat4("model", modelMatrix);

							// Draw
							glDrawArrays(GL_TRIANGLES, 0, 36);
						}

						if (zz == 0 && xx == 0) {
							modelMatrix = glm::mat4(1.f);
							modelMatrix = glm::translate(modelMatrix, glm::vec3(xx * 0.5f + x + (-((time * 0.001) * (time * 0.01) * (time * 0.01))) + random1, 2.5f, zz * 0.5f + z + (-((time * 0.001) * (time * 0.01) * (time * 0.01))) + random2));
							modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(1.f, 0.f, 0.f));
							modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 1.f, 0.f));
							modelMatrix = glm::rotate(modelMatrix, glm::radians(0.f), glm::vec3(0.f, 0.f, 1.f));
							modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
							simpleShader.setMat4("model", modelMatrix);

							// Draw
							glDrawArrays(GL_TRIANGLES, 0, 36);
						}
					}
				}
			
		}

		// draw skybox as last
		glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
		skyboxShader.use();
		viewMatrix = glm::lookAt(glm::vec3(0.f), glm::vec3(0.f) + camFront, worldUp);
		skyboxShader.setMat4("view", viewMatrix);
		skyboxShader.setMat4("projection", projectionMatrix);
		// skybox cube
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // set depth function back to default


		// Swap frameBuffer to current screen
		glfwSwapBuffers(window);
		glFlush();
	}

	// End of program
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}